import * as express from 'express';
import * as request from 'request';
import * as uuid from 'node-uuid';
import * as kue from 'kue';
import * as _ from 'lodash';
import { Database } from 'sqlite3';
import { QueueUtility } from '../utilities/queue-utility';
import { DatabaseUtility } from '../utilities/database-utility';

const databaseUtility = new DatabaseUtility(new Database('./database.sqlite'));
const queueUtility = new QueueUtility(kue.createQueue(), databaseUtility, request.defaults({}), uuid, 10000, 3);
const router = express.Router();

/**
 * GET /?url=http://example.com - used to create job
 */
router.get('/', (req, res, next) => {
  queueUtility.createHtmlJob(req.query.url)
    .then(job => {
      return databaseUtility.insertHtmlRecord(job.data.uuid, JSON.stringify(job));
    })
    .then(record => {
      res.json(record);
    })
    .catch(error => res.json(error));
});

/**
 * GET /:jobId - used for listing job/result with provided jobId
 */
router.get('/:jobId', (req, res, next) => {
  databaseUtility.selectHtmlRecord(req.params.jobId)
    .then(obj => res.json(obj))
    .catch(err => res.json(err));
});

export { router };
