const gulp = require('gulp');
const ts = require('gulp-typescript');
const clean = require('gulp-clean');

const tsProject = ts.createProject('tsconfig.json');

gulp.task('typescript', ['copy:assets'], () => {
    const tsResult = tsProject.src()
        .pipe(tsProject());

    return tsResult.js.pipe(gulp.dest('dist'))
});

gulp.task('copy:assets', ['clean'], () => {
    gulp.src('public/**')
        .pipe(gulp.dest('dist/public'));

    gulp.src('bin/**')
        .pipe(gulp.dest('dist/bin'));

    gulp.src('views/**')
        .pipe(gulp.dest('dist/views'));

    gulp.src('node_modules/**')
        .pipe(gulp.dest('dist/node_modules'));
});

gulp.task('clean', () => {
    return gulp.src('dist/')
        .pipe(clean({
            force: true
        }));
});

gulp.task('build', ['clean', 'copy:assets', 'typescript']);

gulp.task('watch', () => {
    gulp.watch(['views/*', 'app.ts', 'routes/*'], ['build']);
});

gulp.task('default', ['build', 'watch']);