import { RequestAPI, Request, CoreOptions, RequiredUriUrl } from 'request';
import { UUID } from 'node-uuid';
import { Queue, Job }from 'kue';
import { IDatabaseUtility } from './database-utility';

export interface IQueueUtility {
    createHtmlJob(url: string): Promise<Job>;
    isValidUrl(value: string): boolean;
}

export class QueueUtility implements IQueueUtility {
    constructor(private queue: Queue, private databaseUtility: IDatabaseUtility, private request: RequestAPI<Request, CoreOptions, RequiredUriUrl>,
        private uuid: UUID, jobDelayMiliseconds: number = 10000, numParallelJobs: number = 3, private ttlMiliseconds: number = 10000) {

        this.queue.process('html', numParallelJobs, (job, done) => {
            this.processHtmlJob(job, done, jobDelayMiliseconds);
        });
    }

    private processHtmlJob(job, done, delayMilliseconds: number) {
        // Used to simulate a longer running time
        setTimeout(() => {
            this.request.get(job.data.url, (error, response, body) => {
                if (error || response.statusCode !== 200) {
                    this.databaseUtility.updateHtmlRecord(job.data.uuid, JSON.stringify(job), '', JSON.stringify(error))
                        .then(() => done(error || 'Response Code: ${response.statusCode}'))
                        .catch(() => done(error || 'Response Code: ${response.statusCode}'));
                } else {
                    this.databaseUtility.updateHtmlRecord(job.data.uuid, JSON.stringify(job), body, '')
                        .then(() => done())
                        .catch(err => done(err));
                }
            });
        }, delayMilliseconds);
    }

    createHtmlJob(url: string): Promise<Job> {
        return new Promise<Job>((resolve, reject) => {
            if (!url) {
                return reject({
                    message: '"url" query parameter must be provided'
                });
            }

            if (!this.isValidUrl(url)) {
                return reject({
                    message: '"Url" must be a valid Url.'
                });
            }

            const job = this.queue.create('html', {
                url: url,
                uuid: this.uuid.v4()
            }).save(error => {
                if (error) {
                    return reject(error);
                }

                resolve(job);
            }).ttl(this.ttlMiliseconds);
        });

    }

    // Regex from http://stackoverflow.com/questions/8667070/javascript-regular-expression-to-validate-url
    isValidUrl(value: string):boolean {
        if (!value) {
            return false;
        }

        return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value);
    }
}