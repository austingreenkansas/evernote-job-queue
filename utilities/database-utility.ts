import { Database, Statement } from 'sqlite3';

export interface IHtmlRecordRow {
    uuid: string;
    queueJob: string;
    html: string;
    error: string;
}

export interface IJobCreatedMessage {
    jobId: string;
}

// This interface would be used for testing mocks if more time was available
export interface IDatabaseUtility {
    insertHtmlRecord(sqlUuid: string, job: string): Promise<IJobCreatedMessage>;
    updateHtmlRecord(jobId: string, job: string, html: string, error: string): Promise<boolean>;
    selectHtmlRecord(jobId: string): Promise<Object>;
}

export class DatabaseUtility implements IDatabaseUtility {
    private insertStatement: Statement;
    private selectStatement: Statement;
    private updateStatement: Statement;

    constructor(private db: Database) {
        this.db.serialize(() => {
            this.db.run(`CREATE TABLE IF NOT EXISTS 'HtmlJobs' ('uuid' TEXT UNIQUE, 'queueJob'	TEXT, 'html' TEXT, 'error' TEXT, PRIMARY KEY('uuid'));`, (err) => {
                if (err) {
                    throw err;
                }
            });
        });

        this.insertStatement = this.db.prepare("INSERT INTO HtmlJobs (uuid, queueJob, html, error) VALUES ($uuid, $queueJob, $html, $error)");
        this.selectStatement = this.db.prepare("SELECT * FROM HtmlJobs WHERE uuid LIKE $jobId");
        this.updateStatement = this.db.prepare("UPDATE HtmlJobs SET html=$html, queueJob=$job, error=$error WHERE uuid LIKE $jobId");
    }

    insertHtmlRecord(sqlUuid: string, job: string): Promise<IJobCreatedMessage> {
        return new Promise<IJobCreatedMessage>((resolve, reject) => {
            this.insertStatement.run({
                $uuid: sqlUuid,
                $queueJob: JSON.stringify(job),
                $html: '',
                $error: ''
            }, (err) => {
                if(err) {
                    return reject(err);
                }

                resolve({
                    jobId: sqlUuid
                });
            });
        });
    }

    updateHtmlRecord(jobId: string, job: string, html: string, error: string): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            this.updateStatement.run({
                $jobId: `%${jobId}%`,
                $job: job,
                $html: html,
                $error: error
            }, (error) => {
                if (error) {
                    return reject(error);
                }

                resolve(true);
            });
        });
    }

    selectHtmlRecord(jobId: string): Promise<Object> {
        return new Promise<Object>((resolve, reject) => {
            if (!jobId) {
                reject({
                    message: 'No job with provided id found'
                });
            }

            this.selectStatement.get(`%${jobId}%`, (err, row) => {
                const record = row as IHtmlRecordRow;

                if (err) {
                    reject(err);
                }

                if (!row) {
                    return reject({
                        message: 'There is no job with the jobId provided'
                    });
                }

                if (row.error) {
                    return reject({
                        message: 'An error occured while processing your job',
                        error: JSON.parse(row.error)
                    });
                }

                if (row.html) {
                    return resolve({
                        message: 'Job completed',
                        data: record.html
                    });
                }

                resolve({
                    message: 'Job waiting to be processed',
                    queueJob: JSON.parse(record.queueJob)
                });
            });
        });
    }
}