import * as express from 'express';
import * as path from 'path';
import * as serveFavicon from 'serve-favicon';
import * as morgan from 'morgan';
import * as cookieParser from 'cookie-parser';
import * as bodyParser from 'body-parser';
import * as kue from 'kue';

// Routes
import * as index from './routes/index';
import * as job from './routes/job';

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// no favicon for api
app.get('/favicon.ico', function(req, res) {
    res.sendStatus(204);
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Routing for endpoints
app.use('/', index.router);
app.use('/job', job.router);
app.use('/queue', kue.app);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(new Error('Not Found'));
});

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
